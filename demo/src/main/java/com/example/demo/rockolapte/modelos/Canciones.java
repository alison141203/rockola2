/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.rockolapte.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author HP
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="canciones")
public class Canciones implements Serializable{
//    @Autowired
//    transient JdbcTemplate jdbcTempate;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idcanciones")
    private Integer idcanciones;
    @Column(name="nombrecanciones")
    private String nombrecanciones;
    @ManyToOne
    @JoinColumn(name="artistas_idartista")
    private Artistas idartistas;
    @ManyToOne
    @JoinColumn(name="generos_idgeneros")
    private Generos idgeneros;
    @Column(name="anio")
    private Integer anio;
    
    
    
    

    
}
