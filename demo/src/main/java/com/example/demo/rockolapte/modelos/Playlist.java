/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.rockolapte.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="playlist")
public class Playlist implements Serializable {
    
//    @Autowired
//    transient JdbcTemplate jdbcTemplate;
    //Atributos
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idplaylist")
    private Integer idplaylist;//Llave Primaria
    @ManyToOne
    @JoinColumn(name = "canciones_idcanciones")
    private Canciones idcanciones;//Llave Foranea
    @ManyToOne
    @JoinColumn(name = "usuarios_idusuarios")
    private Usuarios idusuarios;//Llave Foranea

}
