/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.rockolapte.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author HP
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="usuarios")
public class Usuarios implements Serializable{
    
//    @Autowired
//    transient JdbcTemplate jdbcTemplate;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idusuarios")
    private Integer idusuarios;
    @Column (name="nombreusuarios")
    private String nombreusuarios;
    @Column (name="apellidousuarios")
    private String apellidousuarios;
    @Column (name="contrasena")
    private float contrasena;
    @Column (name="correo")
    private String correo;
    
}
