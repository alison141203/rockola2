/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

import com.example.demo.rockolapte.modelos.Playlist;
import java.util.List;

public interface Playlistservices {
    
    public Playlist save(Playlist playlist);
    public void delete(Integer id);
    public Playlist findById(Integer id);
    public List<Playlist>findAll();
    
}
