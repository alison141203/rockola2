/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

import com.example.demo.rockolapte.modelos.Generos;
import java.util.List;

/**
 *
 * @author Fransuat
 */
public interface Generosservices {
 
    public Generos save(Generos generos);
    public void delete(Integer id);
    public Generos findById(Integer id);
    public List<Generos>findAll();
}
