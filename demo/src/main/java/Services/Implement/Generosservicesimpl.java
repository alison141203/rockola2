/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services.Implement;

import Services.Generosservices;
import com.example.demo.rockolapte.modelos.Generos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rockolaptedao.Generosdao;

/**
 *
 * @author Fransuat
 */
@Service
public class Generosservicesimpl implements Generosservices {

    @Autowired
    private Generosdao generosdao;

    @Override
    @Transactional(readOnly=false)
    public Generos save(Generos generos) {
        return generosdao.save(generos);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        generosdao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Generos findById(Integer id) {
        return generosdao.findById(id).orElse(null);
    }

    /**
     *
     * @return
     */
    @Override
    @Transactional(readOnly=true)
    public List<Generos> findAll() {
        return (List<Generos>) generosdao.findAll();
    }

}
