/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services.Implement;

import Services.Artistasservices;
import rockolaptedao.Artistasdao;
import com.example.demo.rockolapte.modelos.Artistas;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author HP
 */
@Service
public class Artistasservicesimpl implements Artistasservices {

    @Autowired
    private Artistasdao artistasdao;

    @Override
    @Transactional(readOnly = false)
    public Artistas save(Artistas artistas) {
        return artistasdao.save(artistas);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        artistasdao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Artistas findById(Integer id) {
        return artistasdao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Artistas> findAll() {
        return (List<Artistas>) artistasdao.findAll();
    }
}
