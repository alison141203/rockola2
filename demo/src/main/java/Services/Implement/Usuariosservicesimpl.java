/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services.Implement;

import Services.Usuariosservices;
import com.example.demo.rockolapte.modelos.Usuarios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rockolaptedao.Usuariosdao;
/**
 *
 * @author HP
 */
@Service
public class Usuariosservicesimpl implements Usuariosservices {
    @Autowired
    private Usuariosdao usuariosdao;
    
    @Override
    @Transactional(readOnly=false)
    public Usuarios save(Usuarios usuarios){    
        return usuariosdao.save(usuarios);
        
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        usuariosdao.deleteById(id);
    }
    @Override
    @Transactional(readOnly=true)
    public Usuarios findById(Integer id){
        return usuariosdao.findById(id).orElse(null);    
    }
    @Override
    @Transactional(readOnly=true)
    public List<Usuarios> findAll() {
        return (List<Usuarios>) usuariosdao.findAll();
    }
}
