/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services.Implement;
import Services.Playlistservices;
import com.example.demo.rockolapte.modelos.Playlist;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rockolaptedao.Playlistdao;

@Service 
public class Playlistservicesimpl implements Playlistservices{
    
    @Autowired
    private Playlistdao playlistdao;

    @Override
    @Transactional(readOnly=false)
    public Playlist save(Playlist playlist) {
        return playlistdao.save(playlist);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        playlistdao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Playlist findById(Integer id) {
        return playlistdao.findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly=true)
    public List<Playlist> findAll() {
        return (List<Playlist>) playlistdao.findAll();
    }

}
