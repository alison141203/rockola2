/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services.Implement;

import Services.Cancionesservices;
import com.example.demo.rockolapte.modelos.Canciones;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rockolaptedao.Cancionesdao;


/**
 *
 * @author HP
 */
@Service
public class Cancionesservicesimpl implements Cancionesservices {
    
    @Autowired
    private Cancionesdao cancionesdao;

    @Override
    @Transactional(readOnly=false)
    public Canciones save(Canciones canciones) {
        return cancionesdao.save(canciones);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        cancionesdao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Canciones findById(Integer id) {
        return cancionesdao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Canciones> findAll() {
        return (List<Canciones>) cancionesdao.findAll();
    }
    
}
