/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;
import com.example.demo.rockolapte.modelos.Usuarios;
import java.util.List;
/**
 *
 * @author HP
 */
public interface Usuariosservices {
    public Usuarios save(Usuarios usuarios);
    public void delete(Integer id);
    public Usuarios findById(Integer id);
    public List<Usuarios>findAll();
}
