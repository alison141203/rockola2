/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

import com.example.demo.rockolapte.modelos.Canciones;
import java.util.List;

/**
 *
 * @author HP
 */
public interface Cancionesservices {
    
    public Canciones save(Canciones canciones);
    public void delete(Integer id);
    public Canciones findById(Integer id);
    public List<Canciones>findAll();
}
