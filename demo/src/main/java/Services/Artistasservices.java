/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

import com.example.demo.rockolapte.modelos.Artistas;
import java.util.List;
/**
 *
 * @author HP
 */
public interface Artistasservices {
    
    public Artistas save(Artistas artista);
    public void delete(Integer id);
    public Artistas findById(Integer id);
    public List<Artistas>findAll();
    
}
