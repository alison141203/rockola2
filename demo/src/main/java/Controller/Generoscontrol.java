/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import com.example.demo.rockolapte.modelos.Generos;
import Services.Generosservices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Fransuat2
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/generos")
public class Generoscontrol {
  @Autowired
private Generosservices generosservices;
/*
@PostMapping (value="/")
public ResponseEntity<Generos>agregar(@RequestBody Generos generos){
    Generos obj=generosservices.save(generos);
    
    return new ResponseEntity<>(obj, HttpStatus.OK);
}
@DeleteMapping (value="/{id}")
public ResponseEntity<Generos>eliminar(@PathVariable Integer id){
    Generos obj=generosservices.findById(id);
    
    if (obj!=null)
        generosservices.delete(id);
    
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);  
}
@PutMapping (value="/")
public ResponseEntity<Generos>editar(@RequestBody Generos generos){
    Generos obj=generosservices.findById(generos.getIdgeneros());
    if (obj!=null)
    {
        obj.setCategorias(generos.getCategorias());
        obj.setNombregenero(generos.getNombregenero());
        obj.setTipogenero(generos.getTipogenero());
        generosservices.save(obj);
    }
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);      
}
*/
@GetMapping ("/list")
public List<Generos>consultarTodo(){
    return generosservices.findAll();
}
@GetMapping ("/list/{id}")
public Generos consultarPorId(@PathVariable Integer id){
    return generosservices.findById(id);
}
}
