/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import com.example.demo.rockolapte.modelos.Usuarios;
import Services.Usuariosservices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author HP
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/usuarios")
public class Usuarioscontrol {

    @Autowired
    private Usuariosservices usuariosservices;

    /*@PostMapping(value = "/")
    public ResponseEntity<Usuarios> agregar(@RequestBody Usuarios usuarios) {
        Usuarios obj = usuariosservices.save(usuarios);
        return new ResponseEntity<>(obj, HttpStatus.OK);

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Usuarios> eliminar(@PathVariable Integer id) {
        Usuarios obj = usuariosservices.findById(id);
        if (obj != null) {
            usuariosservices.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Usuarios> editar(@RequestBody Usuarios usuarios) {
        Usuarios obj = usuariosservices.findById(usuarios.getIdusuarios());
        if (obj != null) {
            obj.setNombreusuarios(usuarios.getNombreusuarios());
            obj.setApellidousuarios(usuarios.getApellidousuarios());
            obj.setContrasena(usuarios.getContrasena());
            obj.setCorreo(usuarios.getCorreo());
            obj.setIdplaylist(usuarios.getIdplaylist());
            usuariosservices.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
*/
    @GetMapping("/list")
    public List<Usuarios> consultarTodo() {
        return usuariosservices.findAll();
    }

    @GetMapping("/list/{id}")
    public Usuarios consultarPorId(@PathVariable Integer id) {
        return usuariosservices.findById(id);
    }
}

