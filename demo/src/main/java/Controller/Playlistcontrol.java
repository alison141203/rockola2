/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Services.Playlistservices;
import com.example.demo.rockolapte.modelos.Playlist;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/playlist")
public class Playlistcontrol {

    @Autowired
    private Playlistservices playlistservices;

    /*@PostMapping(value = "/")
    public ResponseEntity<Playlist> agregar(@RequestBody Playlist playlist) {
        Playlist obj = playlistservices.save(playlist);

        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Playlist> eliminar(@PathVariable Integer id) {
        Playlist obj = playlistservices.findById(id);

        if (obj != null) {
            playlistservices.delete(id);
        } else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(obj, HttpStatus.OK);  
        

    }

    @PutMapping(value = "/")
    public ResponseEntity<Playlist> editar(@RequestBody Playlist playlist) {
        Playlist obj = playlistservices.findById(playlist.getIdplaylist());
        if (obj != null) {
            obj.setIdplaylist(playlist.getIdplaylist());
            obj.setIdartistas(playlist.getIdartistas());
            obj.setIdcanciones(playlist.getIdcanciones());
            obj.setIdgeneros(playlist.getIdgeneros());

            playlistservices.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
*/
    @GetMapping("/list")
    public List<Playlist> consultarTodo() {
        return playlistservices.findAll();
    }

    @GetMapping("/list/{id}")
    public Playlist consultarPorId(@PathVariable Integer id) {
        return playlistservices.findById(id);
    }
}
