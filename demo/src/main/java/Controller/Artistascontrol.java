/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import com.example.demo.rockolapte.modelos.Artistas;
import Services.Artistasservices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Fransuat
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/artistas")
public class Artistascontrol {

    @Autowired
    private Artistasservices artistasservices;

    /*@PostMapping (value="/")
public ResponseEntity<Artistas>agregar(@RequestBody Artistas artistas){
    Artistas obj=artistasservices.save(artistas);
    
    return new ResponseEntity<>(obj, HttpStatus.OK);
}
@DeleteMapping (value="/{id}")
public ResponseEntity<Artistas>eliminar(@PathVariable Integer id){
    Artistas obj=artistasservices.findById(id);
    
    if (obj!=null)
        artistasservices.delete(id);
    
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);  
}
@PutMapping (value="/")
public ResponseEntity<Artistas>editar(@RequestBody Artistas artistas){
    Artistas obj=artistasservices.findById(artistas.getIdartista());
    if (obj!=null)
    {
        obj.setNombreartista(artistas.getNombreartista());
        obj.setPaisartista(artistas.getPaisartista());
        artistasservices.save(obj);
    }
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);      
}
     */
    @GetMapping("/saludo")  
    public String saludo(){
        return "hola";
    }
  
  

    @GetMapping("/list")
    public List<Artistas> consultarTodo() {
        return artistasservices.findAll();
    }

    @GetMapping("/list/{id}")
    public Artistas consultarPorId(@PathVariable Integer id) {
        return artistasservices.findById(id);
    }
}
