/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rockolaptedao;

import com.example.demo.rockolapte.modelos.Playlist;
import org.springframework.data.repository.CrudRepository;

public interface Playlistdao extends
    CrudRepository<Playlist,Integer>{
    
}

