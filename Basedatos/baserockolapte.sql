-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: rockolapte
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artistas`
--

DROP TABLE IF EXISTS `artistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artistas` (
  `Id_artista` int NOT NULL AUTO_INCREMENT,
  `nombre_artista` varchar(45) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `generos_Id_generos` int NOT NULL,
  PRIMARY KEY (`Id_artista`,`generos_Id_generos`),
  KEY `fk_artistas_generos1_idx` (`generos_Id_generos`),
  CONSTRAINT `fk_artistas_generos1` FOREIGN KEY (`generos_Id_generos`) REFERENCES `generos` (`Id_generos`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artistas`
--

LOCK TABLES `artistas` WRITE;
/*!40000 ALTER TABLE `artistas` DISABLE KEYS */;
INSERT INTO `artistas` VALUES (10,'shakira','colombia',1),(11,'JbalbinBunny','guadalajara',2);
/*!40000 ALTER TABLE `artistas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `canciones`
--

DROP TABLE IF EXISTS `canciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `canciones` (
  `Id_canciones` int NOT NULL AUTO_INCREMENT,
  `nombre_canciones` varchar(45) NOT NULL,
  `anio` int NOT NULL,
  `generos_Id_generos` int NOT NULL,
  `artistas_Id_artista` int NOT NULL,
  PRIMARY KEY (`Id_canciones`,`generos_Id_generos`,`artistas_Id_artista`),
  KEY `fk_canciones_generos1_idx` (`generos_Id_generos`),
  KEY `fk_canciones_artistas1_idx` (`artistas_Id_artista`),
  CONSTRAINT `fk_canciones_artistas1` FOREIGN KEY (`artistas_Id_artista`) REFERENCES `artistas` (`Id_artista`),
  CONSTRAINT `fk_canciones_generos1` FOREIGN KEY (`generos_Id_generos`) REFERENCES `generos` (`Id_generos`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canciones`
--

LOCK TABLES `canciones` WRITE;
/*!40000 ALTER TABLE `canciones` DISABLE KEYS */;
INSERT INTO `canciones` VALUES (1,'wakawaka',2022,1,10),(2,'faja',1990,2,11);
/*!40000 ALTER TABLE `canciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generos`
--

DROP TABLE IF EXISTS `generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `generos` (
  `Id_generos` int NOT NULL AUTO_INCREMENT,
  `nombre_genero` varchar(45) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `categorias` varchar(45) NOT NULL,
  `tipo_genero` varchar(45) NOT NULL,
  PRIMARY KEY (`Id_generos`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generos`
--

LOCK TABLES `generos` WRITE;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;
INSERT INTO `generos` VALUES (1,'cumbia','colombia','tropical','bailable'),(2,'carranga','rusia','electronica','emo');
/*!40000 ALTER TABLE `generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist`
--

DROP TABLE IF EXISTS `playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `playlist` (
  `Id_playlist` int NOT NULL AUTO_INCREMENT,
  `canciones_Id_canciones` int NOT NULL,
  `canciones_generos_Id_generos` int NOT NULL,
  `canciones_artistas_Id_artista` int NOT NULL,
  PRIMARY KEY (`Id_playlist`,`canciones_Id_canciones`,`canciones_generos_Id_generos`,`canciones_artistas_Id_artista`),
  KEY `fk_playlist_canciones1_idx` (`canciones_Id_canciones`,`canciones_generos_Id_generos`,`canciones_artistas_Id_artista`),
  CONSTRAINT `fk_playlist_canciones1` FOREIGN KEY (`canciones_Id_canciones`, `canciones_generos_Id_generos`, `canciones_artistas_Id_artista`) REFERENCES `canciones` (`Id_canciones`, `generos_Id_generos`, `artistas_Id_artista`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist`
--

LOCK TABLES `playlist` WRITE;
/*!40000 ALTER TABLE `playlist` DISABLE KEYS */;
INSERT INTO `playlist` VALUES (1,1,1,10),(2,2,2,11);
/*!40000 ALTER TABLE `playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `Id_usuarios` int NOT NULL AUTO_INCREMENT,
  `nombre_usuarios` varchar(45) NOT NULL,
  `apellido_usuarios` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `playlist_Id_playlist` int NOT NULL,
  PRIMARY KEY (`Id_usuarios`,`playlist_Id_playlist`),
  KEY `fk_usuarios_playlist_idx` (`playlist_Id_playlist`),
  CONSTRAINT `fk_usuarios_playlist` FOREIGN KEY (`playlist_Id_playlist`) REFERENCES `playlist` (`Id_playlist`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Yhary','Arias','1234','formador59@gmail.com',1),(2,'Nicol','Muñoz','4567','alisonM@gmail.com',2);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-16 14:06:24
